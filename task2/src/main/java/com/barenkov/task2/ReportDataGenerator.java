package com.barenkov.task2;

import com.barenkov.common.Operation;

import java.math.BigDecimal;
import java.util.*;
import java.util.stream.Collectors;

public class ReportDataGenerator {

    public static List<GroupByDateReportLine> generateByDateReportData(List<Operation> operations) {

        return operations.stream()
                .map(op -> new GroupByDateReportLine(op.getOperationDateTime().toLocalDate(), op.getAmount()))
                .collect(Collectors.toList()).stream()
                .collect(Collectors.toMap(GroupByDateReportLine::getOpDate, GroupByDateReportLine::getAmount, BigDecimal::add))
                .entrySet().stream().map(op -> new GroupByDateReportLine(op.getKey(), op.getValue()))
                .sorted(Comparator.comparing(GroupByDateReportLine::getOpDate))
                .collect(Collectors.toList());

    }

    public static List<GroupByOfficeReportLine> generateByOfficeReportData(List<Operation> operations) {

        return operations.stream()
                .map(emp -> new GroupByOfficeReportLine(emp.getOffice(), emp.getAmount()))
                .collect(Collectors.toList())
                .stream()
                .collect(Collectors.toMap(GroupByOfficeReportLine::getOffice, GroupByOfficeReportLine::getAmount, BigDecimal::add))
                .entrySet().stream().map(op -> new GroupByOfficeReportLine(op.getKey(), op.getValue()))
                .sorted(Comparator.comparing(GroupByOfficeReportLine::getAmount).reversed())
                .collect(Collectors.toList());


    }


}
