package com.barenkov.task2;


import lombok.Getter;
import lombok.Setter;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;

import java.util.List;

@Getter
@Setter
public class Report<T>  {

    private String fileName;
    List<T> groupedOperations;

    public void saveReport() {
        try (BufferedWriter writer = new BufferedWriter(new FileWriter(fileName))) {
            for (T op : groupedOperations) {
                writer.write(op.toString());
                writer.write(System.lineSeparator());
            }
            System.out.println("Saving report file: + " + fileName);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}





