package com.barenkov.task2;

import lombok.AllArgsConstructor;
import lombok.Data;

import java.math.BigDecimal;

@Data
@AllArgsConstructor
public class GroupByOfficeReportLine {

        private String office;
        private BigDecimal amount;

    @Override
    public String toString() {
        return  office + '\t' + amount;
    }
}



