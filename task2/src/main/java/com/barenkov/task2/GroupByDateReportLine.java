package com.barenkov.task2;

import com.barenkov.common.DateTimeUtils;
import lombok.*;

import java.math.BigDecimal;
import java.time.LocalDate;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class GroupByDateReportLine {

        private LocalDate opDate;
        private BigDecimal amount;

    @Override
    public String toString() {
        return  opDate.format(DateTimeUtils.yyyyMMDD_formatter)  + '\t' + amount;
    }
    }



