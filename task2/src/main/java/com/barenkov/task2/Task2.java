package com.barenkov.task2;

import com.barenkov.common.DateTimeUtils;
import com.barenkov.common.Operation;

import java.io.IOException;
import java.math.BigDecimal;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.*;

public class Task2 {

    public static List<Operation> operationList = new ArrayList<>();

    public static void main(String... args) {

        if (args.length < 3) {
            System.err.println("You must specify at least 3 args: stats-dates, stats-offices and operations file(s)");
            System.exit(1);
        }

        int inputFilesCount = args.length - 2;

        for (int i = 2; i <= inputFilesCount + 1; i++) {
            loadOperatons(args[i]);
        }

        List<Operation> operations = Collections.unmodifiableList(operationList);

        Report<GroupByDateReportLine> byDateReport = new Report<>();
        byDateReport.setFileName(args[0]);
        byDateReport.setGroupedOperations(ReportDataGenerator.generateByDateReportData(operations));
        byDateReport.saveReport();

        Report<GroupByOfficeReportLine> byOfficeReport = new Report<>();
        byOfficeReport.setFileName(args[1]);
        byOfficeReport.setGroupedOperations(ReportDataGenerator.generateByOfficeReportData(operations));
        byOfficeReport.saveReport();

    }

    private static void loadOperatons(String fileName) {
        try {
            System.out.println("Load operations file " + fileName);
            List<String> productLines = Files.readAllLines(Paths.get(fileName), StandardCharsets.UTF_8);
            productLines.forEach(line -> {
                Operation operation = new Operation();
                String[] tokens = line.split("\t");
                operation.setOperationDateTime(DateTimeUtils.fromString(tokens[0]));
                operation.setOffice(tokens[1]);
                operation.setOperationNumber(tokens[2]);
                operation.setAmount(amountObjCache.getInstance(tokens[3]));
                operationList.add(operation);
            });
        } catch (IOException e) {
            System.err.println("Can't load file " + fileName);
        }
    }

    private static class amountObjCache {
        static Map<String, BigDecimal> cache = new HashMap<>();

        public static BigDecimal getInstance(String amount) {
            BigDecimal result = cache.get(amount);
            if (result == null) {
                result = new BigDecimal(amount);
                cache.put(amount, result);
            }
            return result;
        }

    }
}
