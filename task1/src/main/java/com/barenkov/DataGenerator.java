package com.barenkov;

import com.barenkov.common.Operation;
import com.barenkov.common.RandomUtils;

import java.util.ArrayList;
import java.util.List;

public class DataGenerator {

    public static List<Operation> createOperations(int listSize, List<String> offices, int year) {

        List<Operation> operations = new ArrayList<>();
        int i = 1;
        while (i <= listSize) {
            int index = RandomUtils.getRandomIndex(offices.size());
            Operation operation = OperationFactory.getInstance(offices.get(index),year);
            operations.add(operation);
            i++;
        }
        return operations;
    }





}
