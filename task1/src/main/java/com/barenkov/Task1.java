package com.barenkov;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.time.LocalDateTime;
import java.util.Collections;
import java.util.List;

public class Task1 {

    private static final int LAST_YEAR = LocalDateTime.now().getYear() - 1;

    public static void main(String... args) {

        if (args.length < 3) {
            System.err.println("You must specify at least 3 args: offices file, operation count and result file(s)");
            System.exit(1);
        }

        String officesFile = args[0];
        int totalOpCount = 0;
        int resultFilesCount = args.length - 2;

        try {
            totalOpCount = Integer.parseInt(args[1]);
        } catch (NumberFormatException e) {
            System.err.println("Argument" + args[1] + " must be an integer.");
            System.exit(1);
        }

        List<String> officesList = null;
        try {
            officesList = Files.readAllLines(Paths.get(officesFile), StandardCharsets.UTF_8);
        } catch (IOException e) {
            System.err.println("Can't open file " + officesFile);
            System.exit(1);
        }


        for (int i = 1; i <= resultFilesCount; i++) {
            OperationReport report = new OperationReport();
            report.setFileName(args[i + 1]);
            int nOfLines = calcOpCountPerReport(totalOpCount, resultFilesCount, i);
            report.setOperationList(DataGenerator.createOperations(nOfLines, Collections.unmodifiableList(officesList), LAST_YEAR));
            report.saveReport();
        }
    }

    private static int calcOpCountPerReport(int totalOpCount, int resultFilesCount, int currentFile) {
        if (resultFilesCount == 1)
            return totalOpCount;
        else {
            int batchSize = totalOpCount / resultFilesCount;
            if (currentFile - resultFilesCount == 0) {
                return totalOpCount - (batchSize * (resultFilesCount - 1));
            }
            return batchSize;
        }


    }


}






