package com.barenkov;

import com.barenkov.common.DateTimeUtils;
import com.barenkov.common.Operation;

import java.math.BigDecimal;
import java.util.*;

import static com.barenkov.common.RandomUtils.getRandomIndex;

public class OperationFactory {

    public static List<BigDecimal> amountObjMap = new ArrayList<>();

    static {
        BigDecimal baseAmount = BigDecimal.valueOf(100000);
        for (int i = 12; i <= 50; i++) {
            BigDecimal fraction = new BigDecimal("0." + i);
            amountObjMap.add(baseAmount.add(fraction));
        }

    }

    public static Operation getInstance(String office, int year) {
        Operation operation = new Operation();
        operation.setOperationNumber(String.valueOf(UUID.randomUUID()));
        operation.setAmount(amountObjMap.get(getRandomIndex(amountObjMap.size())));
        operation.setOffice(office);
        operation.setOperationDateTime(DateTimeUtils.getRandomOperationDate(year));
        return operation;
    }


}
