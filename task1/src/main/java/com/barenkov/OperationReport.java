package com.barenkov;

import com.barenkov.common.Operation;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.util.List;


public class OperationReport {

    String fileName;
    List<Operation> operationList;

    public void saveReport() {

        try (BufferedWriter writer = new BufferedWriter(new FileWriter(fileName))) {
            for (Operation op : operationList) {
                writer.write(op.toString());
                writer.write(System.lineSeparator());
            }
            System.out.println("Saving report file: " + fileName);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    public void setOperationList(List<Operation> operationList) {
        this.operationList = operationList;
    }
}
