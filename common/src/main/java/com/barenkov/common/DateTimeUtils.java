package com.barenkov.common;

import java.time.LocalDateTime;
import java.time.Month;
import java.time.ZoneOffset;
import java.time.format.DateTimeFormatter;
import java.util.concurrent.ThreadLocalRandom;

public class DateTimeUtils {

    public static DateTimeFormatter yyyyMMDD_HHmmss_formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
    public static DateTimeFormatter yyyyMMDD_formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");


    public static LocalDateTime getRandomOperationDate(int year) {
        LocalDateTime startDate = LocalDateTime.of(year, Month.JANUARY, 1, 0,0,0);
        LocalDateTime endDate =   LocalDateTime.of(year, Month.DECEMBER, 31, 23,59,59);

        long startEpochSeconds = startDate.toEpochSecond(ZoneOffset.UTC);
        long endEpochSeconds = endDate.toEpochSecond(ZoneOffset.UTC);

        long randomDay = ThreadLocalRandom
                .current()
                .nextLong(startEpochSeconds, endEpochSeconds);

        return LocalDateTime.ofEpochSecond(randomDay, 0, ZoneOffset.UTC);

    }

    public static LocalDateTime fromString(String s) {

        return LocalDateTime.parse(s, DateTimeUtils.yyyyMMDD_HHmmss_formatter);

    }

}
