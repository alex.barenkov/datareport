package com.barenkov.common;

import java.util.concurrent.ThreadLocalRandom;

public class RandomUtils {

    public static int getRandomIndex(int bound) {
        return ThreadLocalRandom.current().nextInt(bound);

    }
}
