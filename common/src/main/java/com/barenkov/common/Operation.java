package com.barenkov.common;

import lombok.Data;

import java.math.BigDecimal;
import java.time.LocalDateTime;

@Data
public class Operation {

    LocalDateTime operationDateTime;
    String office;
    String operationNumber;
    BigDecimal amount;

    @Override
    public String toString() {
        return operationDateTime.format(DateTimeUtils.yyyyMMDD_HHmmss_formatter) + "\t" + office + '\t' + operationNumber + '\t' + amount;
    }

}
